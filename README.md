# Aci payment system

## How to download this project

#### 1. Install git on your PC.

#### 2. Open terminal in empty folder and use this command to download the project.

`git clone https://GASERO@bitbucket.org/GASERO/acitask.git` 

## Server

#### 0. At first you should make sure that java and maven are already installed on your PC.

#### 1. To run server open terminal in "server" folder.

#### 2. Run server by this command. Server will automatically start on 8080 port.

` mvn spring-boot:run `

## Client

#### 0. At first you should make sure that Node.js is already installed on your PC.

#### 1. To run client open terminal in "client" folder.

#### 2. Run client application by this command. Client-application will automatically start on 4200 port

`ng serve`
