package ru.aciworldwide.service;

import ru.aciworldwide.model.Biller;

import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 17.05.2018
 */
public interface BillerService {
    Biller findById(Long id);
    void save(Biller biller);
    void delete(Biller biller);
    List<Biller> findAll();
}
