package ru.aciworldwide.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.aciworldwide.model.Biller;
import ru.aciworldwide.repositories.BillerRepository;
import ru.aciworldwide.repositories.PaymentReposiroty;
import ru.aciworldwide.service.BillerService;

import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 17.05.2018
 */
@Service
public class BillerServiceImpl implements BillerService {

    private BillerRepository billerRepository;
    private PaymentReposiroty paymentReposiroty;

    public BillerServiceImpl(BillerRepository billerRepository, PaymentReposiroty paymentReposiroty) {
        this.billerRepository = billerRepository;
        this.paymentReposiroty = paymentReposiroty;
    }

    @Override
    public Biller findById(Long id) {
        return billerRepository.getOne(id);
    }

    @Override
    public void save(Biller biller) {
        billerRepository.save(biller);
    }

    @Transactional
    @Override
    public void delete(Biller biller) {
        paymentReposiroty.deleteAllByBiller(biller);
        billerRepository.delete(biller);
    }

    @Override
    public List<Biller> findAll() {
        return billerRepository.findAll();
    }
}
