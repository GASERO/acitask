package ru.aciworldwide.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.aciworldwide.dto.CustomerDto;
import ru.aciworldwide.model.Customer;
import ru.aciworldwide.repositories.CustomerRepository;
import ru.aciworldwide.repositories.PaymentReposiroty;
import ru.aciworldwide.service.CustomerService;

import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 17.05.2018
 */
@Service
public class CustomerServiceImpl implements CustomerService {
    private CustomerRepository customerRepository;
    private PaymentReposiroty paymentReposiroty;

    public CustomerServiceImpl(CustomerRepository customerRepository, PaymentReposiroty paymentReposiroty) {
        this.customerRepository = customerRepository;
        this.paymentReposiroty = paymentReposiroty;
    }
    @Transactional
    @Override
    public void delete(Customer customer) {
        paymentReposiroty.deleteAllByCustomer(customer);
        customerRepository.delete(customer);
    }


    @Override
    public Customer findById(Long id) {
        return customerRepository.getOne(id);
    }


    @Override
    public void save(Customer customer) {
        customerRepository.save(customer);
    }

    @Override
    public List<Customer> findAll() {
        return customerRepository.findAll();
    }
}
