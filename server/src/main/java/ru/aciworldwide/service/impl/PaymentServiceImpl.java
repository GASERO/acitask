package ru.aciworldwide.service.impl;

import org.springframework.stereotype.Service;
import ru.aciworldwide.dto.FilterInfoDto;
import ru.aciworldwide.dto.PaymentDto;
import ru.aciworldwide.model.Payment;
import ru.aciworldwide.repositories.PaymentReposiroty;
import ru.aciworldwide.service.PaymentService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 17.05.2018
 */
@Service
public class PaymentServiceImpl implements PaymentService {
    private PaymentReposiroty paymentReposiroty;

    public PaymentServiceImpl(PaymentReposiroty paymentReposiroty) {
        this.paymentReposiroty = paymentReposiroty;
    }

    @Override
    public void save(Payment payment) {
        paymentReposiroty.save(payment);
    }

    @Override
    public List<Payment> findAll() {
        List<Payment> result = paymentReposiroty.findAll();
        result.sort((o1,o2)->{
            return o2.getDate().compareTo(o1.getDate());
        });
        return result;
    }

    @Override
    public List<Payment> findAllByFilter(FilterInfoDto filter) {
        if (filter.getBillerId() == 0 && filter.getCustomerId() == 0){ // фильтр не стоит
            return  paymentReposiroty.findAll();
        }
        if (filter.getBillerId() == 0 && filter.getCustomerId() !=0){ // фильтр стоит  только на кастомера
            return  paymentReposiroty.findAllByCustomerId(filter.getCustomerId());
        }
        if (filter.getBillerId() !=0 && filter.getCustomerId() ==0){ // фильтр стоит только на биллера
            return paymentReposiroty.findAllByBillerId(filter.getBillerId());
        }
        //если дошел до сюда, то фильтр стоит на обоих
        return paymentReposiroty.findAllByCustomerIdAndBillerId(filter.getCustomerId(),filter.getBillerId());
    }
}
