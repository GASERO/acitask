package ru.aciworldwide.service;

import ru.aciworldwide.dto.CustomerDto;
import ru.aciworldwide.model.Customer;

import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 17.05.2018
 */
public interface CustomerService {
    void delete(Customer customer);
    Customer findById(Long id);
    void save(Customer customer);
    List<Customer> findAll();
}
