package ru.aciworldwide.service;

import ru.aciworldwide.dto.FilterInfoDto;
import ru.aciworldwide.dto.PaymentDto;
import ru.aciworldwide.model.Payment;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 17.05.2018
 */
public interface PaymentService {
    void save(Payment payment);
    List<Payment> findAll();

    List<Payment> findAllByFilter(FilterInfoDto filter);
}
