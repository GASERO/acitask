package ru.aciworldwide.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.aciworldwide.model.Biller;
import ru.aciworldwide.model.Customer;
import ru.aciworldwide.model.Payment;

import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 17.05.2018
 */
public interface PaymentReposiroty extends JpaRepository<Payment,Long> {
    List<Payment> findAllByBillerId(Long id);
    List<Payment> findAllByCustomerId(Long id);
    List<Payment> findAllByCustomerIdAndBillerId(Long customerId,Long billerId);
    void deleteAllByBiller(Biller biller);

    void deleteAllByCustomer(Customer customer);
}
