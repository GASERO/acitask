package ru.aciworldwide.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.aciworldwide.model.Customer;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 17.05.2018
 */
public interface CustomerRepository extends JpaRepository<Customer,Long> {
}
