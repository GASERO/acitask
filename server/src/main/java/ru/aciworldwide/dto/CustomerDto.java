package ru.aciworldwide.dto;

import lombok.*;
import ru.aciworldwide.model.Customer;

import javax.validation.constraints.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 17.05.2018
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
public class CustomerDto {
    private Long id;

    @NotNull
    @NotEmpty
    @Pattern(regexp = "[A-Z][a-z]*")
    @Size(max = 50, min = 1)
    private String firstName;

    @NotNull
    @NotEmpty
    @Pattern(regexp = "[A-Z][a-z]*")
    @Size(max = 50, min = 1)
    private String lastName;

    @NotNull
    private Date dateOfBirth;

    @NotNull
    @NotEmpty
    private String address;

    public static CustomerDto from(Customer customer) {
        return builder()
                .id(customer.getId())
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .dateOfBirth(customer.getDateOfBirth())
                .address(customer.getAddress())
                .build();
    }

    public static List<CustomerDto> from(List<Customer> customers){
        List<CustomerDto> customerDtos = new ArrayList<>();
        customers.forEach(customer -> {
            customerDtos.add(CustomerDto.from(customer));
        });
        return customerDtos;
    }
}
