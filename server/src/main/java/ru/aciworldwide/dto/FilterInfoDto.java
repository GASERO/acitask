package ru.aciworldwide.dto;

import lombok.*;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 19.05.2018
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
public class FilterInfoDto {
    private Long customerId;
    private Long billerId;
}
