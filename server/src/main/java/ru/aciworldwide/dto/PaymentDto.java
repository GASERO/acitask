package ru.aciworldwide.dto;

import lombok.*;
import ru.aciworldwide.model.Biller;
import ru.aciworldwide.model.Customer;
import ru.aciworldwide.model.Payment;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 17.05.2018
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
public class PaymentDto {
    private Long id;

    @NotNull
    private Customer customer;

    @NotNull
    private Biller biller;

    @NotNull
    @NotEmpty
    @Size(max =50)
    @Pattern(regexp = "[0-9]*")
    private String account;

    @NotNull
    @NotEmpty
    @Pattern(regexp = "[1-9][0-9]*(\\.[0-9]{0,2})?")
    private String amount;

    private Date date;

    public static PaymentDto from(Payment payment) {
        return builder()
                .id(payment.getId())
                .customer(payment.getCustomer())
                .biller(payment.getBiller())
                .account(payment.getAccount())
                .amount(payment.getAmount().toString())
                .date(payment.getDate())
                .build();
    }

    public static List<PaymentDto> from(List<Payment> payments){
        List<PaymentDto> paymentDtos = new ArrayList<>();
        payments.forEach(payment -> {
            paymentDtos.add(PaymentDto.from(payment));
        });
        return paymentDtos;
    }
}
