package ru.aciworldwide.dto;

import lombok.*;
import ru.aciworldwide.model.Biller;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 17.05.2018
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
public class BillerDto {
    private Long id;

    @NotNull
    @NotEmpty
    @Pattern(regexp = "[A-Z0-9][\\w ]*")
    private String companyName;

    public static BillerDto from(Biller biller) {
        return builder()
                .id(biller.getId())
                .companyName(biller.getCompanyName())
                .build();
    }

    public static List<BillerDto> from(List<Biller> billers){
        List<BillerDto> billerDtos = new ArrayList<>();
        billers.forEach(biller -> {
            billerDtos.add(BillerDto.from(biller));
        });
        return billerDtos;
    }
}
