package ru.aciworldwide.controllers.rest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.aciworldwide.dto.BillerDto;
import ru.aciworldwide.model.Biller;
import ru.aciworldwide.service.BillerService;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 17.05.2018
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("api/billers")
public class BillerController {
    private BillerService billerService;

    public BillerController(BillerService billerService) {
        this.billerService = billerService;
    }

    @GetMapping("")
    public List<BillerDto> getBillers() {
        return BillerDto.from(billerService.findAll());
    }

    @GetMapping("/{id}")
    public BillerDto getBiller( @PathVariable("id") Long id) {
        return BillerDto.from(billerService.findById(id));
    }

    @PostMapping("")
    public void addNewBiller(@RequestBody @Valid BillerDto billerDto, BindingResult errors) {
        if (errors.hasErrors()) {
            return;
        }
        billerService.save(Biller.builder()
                .companyName(billerDto.getCompanyName())
                .build());
    }

    @PutMapping("/{id}")
    public void editBiller(@RequestBody @Valid BillerDto billerDto,@PathVariable("id") Long id) {
        Biller biller = billerService.findById(id);
        biller.setCompanyName(billerDto.getCompanyName());
        billerService.save(biller);
    }

    @DeleteMapping("/{id}")
    public List<BillerDto> deleteBiller(@PathVariable("id") Long id) {
        Biller biller = billerService.findById(id);
        billerService.delete(biller);
        return BillerDto.from(billerService.findAll());
    }
}
