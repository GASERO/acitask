package ru.aciworldwide.controllers.rest;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.aciworldwide.dto.FilterInfoDto;
import ru.aciworldwide.dto.PaymentDto;
import ru.aciworldwide.model.Payment;
import ru.aciworldwide.service.PaymentService;

import javax.validation.Valid;
import java.sql.Date;
import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 17.05.2018
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("api/payments")
public class PaymentController {
    private final PaymentService paymentService;

    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @GetMapping("")
    public List<PaymentDto> getAllPayments() {
        return PaymentDto.from(paymentService.findAll());
    }

    @PostMapping("")
    public void addNewPayment(@RequestBody @Valid PaymentDto paymentDto, BindingResult errors) {
        System.out.println(paymentDto.toString());
        System.out.println(errors.getAllErrors().toString());
        if (errors.hasErrors()) {
           return;
        }
        paymentService.save(Payment.builder()
                .customer(paymentDto.getCustomer())
                .biller(paymentDto.getBiller())
                .account(paymentDto.getAccount())
                .amount(Double.parseDouble(paymentDto.getAmount()))
                .date(new Date(System.currentTimeMillis()))
                .build());
    }

    @PostMapping("/filter")
    public List<PaymentDto> getPaymentsByFilter(@Valid @RequestBody FilterInfoDto filter) {
        System.out.println(filter.toString());
        return PaymentDto.from(paymentService.findAllByFilter(filter));
    }
}
