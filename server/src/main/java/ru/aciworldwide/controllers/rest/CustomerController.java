package ru.aciworldwide.controllers.rest;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import ru.aciworldwide.dto.CustomerDto;
import ru.aciworldwide.model.Customer;
import ru.aciworldwide.service.CustomerService;
import sun.misc.IOUtils;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.sql.Date;
import java.util.List;


/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 17.05.2018
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("api/customers")
public class CustomerController {
    private CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("")
    public List<CustomerDto> getCustomers() {
        return CustomerDto.from(customerService.findAll());
    }

    @GetMapping("/{id}")
    public CustomerDto getCustomer(@PathVariable("id") Long id) {
        return CustomerDto.from(customerService.findById(id));
    }

    @PostMapping("")
    public void addNewCustomer(@Valid @RequestBody CustomerDto customerDto, BindingResult errors) {
        if (errors.hasErrors()) {
            return;
        }
        if (customerDto.getDateOfBirth().after(new Date(System.currentTimeMillis()))){ //Если кастомер из будущего))
            return;
        }
        Customer customer = Customer.builder()
                .firstName(customerDto.getFirstName())
                .lastName(customerDto.getLastName())
                .dateOfBirth(customerDto.getDateOfBirth())
                .address(customerDto.getAddress())
                .build();
        customerService.save(customer);
    }

    @PutMapping("/{id}")
    public void editCustomer(@RequestBody @Valid CustomerDto customerDto,
                               @PathVariable("id") Long id) {
        Customer customer = customerService.findById(id);
        customer.setFirstName(customerDto.getFirstName());
        customer.setLastName(customerDto.getLastName());
        customer.setDateOfBirth(customerDto.getDateOfBirth());
        customer.setAddress(customerDto.getAddress());
        customerService.save(customer);
    }

    @DeleteMapping("/{id}")
    public List<CustomerDto> deleteCustomer(@PathVariable("id") Long id) {
        Customer customer = customerService.findById(id);
        customerService.delete(customer);
        return CustomerDto.from(customerService.findAll());
    }
}
