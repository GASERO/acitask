import { Component, OnInit } from '@angular/core';
import {BillersService} from '../services/biller-service/biller-service.service';

@Component({
  selector: 'app-billers',
  templateUrl: './billers.component.html',
  styleUrls: ['./billers.component.scss']
})
export class BillersComponent implements OnInit {

  billers: Array<any>;

  constructor(private billerService: BillersService) { }

  ngOnInit() {
    this.billerService.getAll().subscribe(
      billers => this.billers = billers
    );
  }

  deleteBiller(id: string) {
    this.billerService.delete(id).subscribe(
      billers => this.billers = billers
    );
  }

}
