import { Component, OnInit } from '@angular/core';
import {PaymentService} from '../services/payment-service/payment.service';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {
  payments: Array<any>;
  customers: Array<any>;
  billers: Array<any>;

  billerIdFilter: number = 0;
  customerIdFilter: number = 0;

  constructor(private paymentService: PaymentService) { }

  ngOnInit() {
    this.paymentService.getAll().subscribe(
      payments => this.payments = payments
    );
    this.paymentService.getCustomersAndBillers().subscribe(data => {
      this.customers = data[0];
      this.billers = data[1];
    });
  }

  updatePayments() {
    let filter = {
      billerId: this.billerIdFilter,
      customerId: this.customerIdFilter
    };
    this.paymentService.getPaymentsByFilter(filter).subscribe(payments => this.payments = payments);
  }

  isPaymentAddingSuccessful() {
    return this.paymentService.addingFlag == 1;
  }

  isPaymentAddingFailed() {
    return this.paymentService.addingFlag == -1;
  }

  closeMessage() {
    this.paymentService.addingFlag = 0;
  }

}
