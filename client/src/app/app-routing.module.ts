import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomersComponent } from './customers/customers.component';
import { BillersComponent } from './billers/billers.component';
import { PaymentsComponent } from './payments/payments.component';
import { CustomerEditComponent} from './customer-edit/customer-edit.component';
import { PaymentAddComponent } from './payment-add/payment-add.component';
import {PageNotFoundComponentComponent} from './page-not-found-component/page-not-found-component.component';
import {BillerEditComponent} from './biller-edit/biller-edit.component';



const routes: Routes = [
  {
    path:'payments',
    component: PaymentsComponent
  },
  {
    path:'customers',
    component: CustomersComponent
  },
  {
    path:'billers',
    component: BillersComponent
  },
  {
    path:'customer/add',
    component: CustomerEditComponent
  },
  {
    path:'payment/add',
    component: PaymentAddComponent
  },
  {
    path:'customer/edit/:id',
    component: CustomerEditComponent
  },

  {
    path:'biller/edit/:id',
    component: BillerEditComponent
  },
  {
    path:'biller/add',
    component: BillerEditComponent
  },
  {
    path:'error',
    component: PageNotFoundComponentComponent
  },
  {
    path:'**',
    component: PageNotFoundComponentComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
