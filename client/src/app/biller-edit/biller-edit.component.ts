import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs/Subscription";
import {NgForm} from "@angular/forms";
import {BillersService} from '../services/biller-service/biller-service.service';

@Component({
  selector: 'app-biller-edit',
  templateUrl: './biller-edit.component.html',
  styleUrls: ['./biller-edit.component.scss']
})
export class BillerEditComponent implements OnInit, OnDestroy {

  biller: any = {};

  sub: Subscription;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private billerService: BillersService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.billerService.get(id).subscribe((biller: any) => {
            this.biller = biller;
            this.biller.isLoaded = true;
        });
      }
    })
  }

  returnToList() {
    this.router.navigate(['/billers']);
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  save(form: NgForm) {
    this.billerService.save(form).subscribe(
      () => this.returnToList()
    );
  }

  saveForm(form: NgForm) {
    if (form['isLoaded']) {
      this.edit(form);
    } else {
      this.save(form);
    }
  }

  edit(form: NgForm) {
    this.billerService.update(form, this.biller.id).subscribe(
      () => this.returnToList()
    );
  }

  delete(id: string) {
    this.billerService.delete(id).subscribe(
      () => this.returnToList()
    );
  }

}
