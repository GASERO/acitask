import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CustomerService} from '../customer-service/customer-service.service';
import {forkJoin, Observable} from 'rxjs';
import {BillersService} from '../biller-service/biller-service.service';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  public API_URL = '//localhost:8080/api/payments';
  public addingFlag: number = 0;

  constructor(private http: HttpClient,
              private billersService: BillersService,
              private customerService: CustomerService) { }

  getAll(): Observable<any> {
    return this.http.get(this.API_URL);
  }

  save(payment: any) {
    return this.http.post(this.API_URL, payment);
  }

  getCustomersAndBillers() {
    return forkJoin(
      this.customerService.getAll(),
      this.billersService.getAll()
    );
  }

  getPaymentsByFilter(filter: any): Observable<any>{
    return this.http.post(this.API_URL + "/filter", filter)
  }
}
