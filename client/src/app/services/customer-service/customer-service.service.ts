import {HttpClient} from "@angular/common/http";
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';

@Injectable()
export class CustomerService {

  public API_URL = '//localhost:8080/api/customers';

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(this.API_URL);
  }

  get(id: string) {
    return this.http.get(this.API_URL + "/" + id);
  }

  save(customer: any) {
    return this.http.post(this.API_URL, customer);
  }

  delete(id: string): Observable<any> {
    return this.http.delete(this.API_URL + "/" + id);
  }

  update(customer: any, id: string) {
    return this.http.put(this.API_URL + "/" + id, customer);
  }
}
