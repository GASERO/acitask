import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {catchError} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class BillersService {


  public API_URL = '//localhost:8080/api/billers';

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(this.API_URL);
  }

  get(id: string) {
    return this.http.get(this.API_URL + "/" + id);
  }

  delete(id: string): Observable<any> {
    return this.http.delete(this.API_URL + "/" + id);
  }

  update(biller: any, id: string) {
    return this.http.put(this.API_URL + "/" + id, biller);
  }

  save(biller: any): Observable<Object> {
    return this.http.post(this.API_URL, biller);
  }


}
