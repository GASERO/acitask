import { TestBed, inject } from '@angular/core/testing';

import { BillersService } from './biller-service.service';

describe('BillersServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BillersService]
    });
  });

  it('should be created', inject([BillersService], (service: BillersService) => {
    expect(service).toBeTruthy();
  }));
});
