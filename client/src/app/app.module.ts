import { BrowserModule } from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BillersComponent } from './billers/billers.component';
import { PaymentsComponent } from './payments/payments.component';
import { CustomersComponent } from './customers/customers.component';
import { BillerEditComponent } from './biller-edit/biller-edit.component';
import { PaymentAddComponent } from './payment-add/payment-add.component';
import { CustomerEditComponent } from './customer-edit/customer-edit.component';
import { PageNotFoundComponentComponent } from './page-not-found-component/page-not-found-component.component';
import {CustomerService} from './services/customer-service/customer-service.service';
import {HTTP_INTERCEPTORS, HttpClientModule, HttpInterceptor} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {HttpInterceptorHandler} from '@angular/common/http/src/interceptor';
import {RequestInterceptor} from './my-error-handler';

@NgModule({
  declarations: [
    AppComponent,
    BillersComponent,
    PaymentsComponent,
    CustomersComponent,
    BillerEditComponent,
    PaymentAddComponent,
    CustomerEditComponent,
    PageNotFoundComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    CustomerService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi:true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
