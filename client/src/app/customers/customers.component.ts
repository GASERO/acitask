import { Component, OnInit } from '@angular/core';
import { CustomerService} from '../services/customer-service/customer-service.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {

  customers: Array<any>;

  constructor(private customerService: CustomerService) { }

  ngOnInit() {
    this.customerService.getAll().subscribe(
      customers => this.customers = customers
    )
  }

  deleteCustomer(id: string) {
    this.customerService.delete(id).subscribe(
      customers => this.customers = customers
    );
  }

}
