import { Component, OnInit } from '@angular/core';
import {CustomerService} from '../services/customer-service/customer-service.service';
import {NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {parseHttpResponse} from 'selenium-webdriver/http';

@Component({
  selector: 'app-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.scss']
})
export class CustomerEditComponent implements OnInit {
  customer: any={};
  sub: Subscription;

  constructor(private customerService: CustomerService,
              private router: Router,
              private route: ActivatedRoute
              ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.customerService.get(id).subscribe((customer: any) => {
            this.customer = customer;
            this.customer.isLoaded = true;
        });
      }
    })
  }
  saveForm(form: NgForm){
    console.log(form);
    if (form['isLoaded']){
      this.edit(form);
    }else{
      this.save(form);
    }
  }

  save(form: NgForm) {
    this.customerService.save(form).subscribe(
      () => this.returnToList()
    );
  }

  edit(form: NgForm) {
    this.customerService.update(form, this.customer.id).subscribe(
      () => this.returnToList()
    );
  }

  returnToList() {
    this.router.navigate(['/customers']);
  }

  delete(id: string) {
    this.customerService.delete(id).subscribe(
      () => this.returnToList()
    );
  }


}
