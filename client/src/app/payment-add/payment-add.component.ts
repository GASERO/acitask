import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {PaymentService} from '../services/payment-service/payment.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-payment-edit',
  templateUrl: './payment-add.component.html',
  styleUrls: ['./payment-add.component.scss']
})
export class PaymentAddComponent implements OnInit {
  payment: any = {};
  customers: Array<any>;
  billers: Array<any>;

  constructor(private router: Router, private paymentService: PaymentService) {
  }

  ngOnInit() {
    this.paymentService.getCustomersAndBillers().subscribe(data => {
      this.customers = data[0];
      this.billers = data[1];
    });
  }

  savePaymentForm(form: NgForm) {
    this.paymentService.save(form).subscribe(
      () => this.successfulPaymentAdding(),
      error => this.failedPaymentAdding(error)
    )
  }

  returnToList() {
    this.router.navigate(['/payments']);
  }

  private successfulPaymentAdding() {
    this.paymentService.addingFlag = 1;
    this.returnToList();
  }

  private failedPaymentAdding(error: any) {
    this.paymentService.addingFlag = -1;
    this.returnToList();
  }
}
